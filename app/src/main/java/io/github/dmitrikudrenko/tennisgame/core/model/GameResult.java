package io.github.dmitrikudrenko.tennisgame.core.model;


public class GameResult {
    private GameScore score;
    private Player winner;

    public GameResult(GameScore score, Player winner) {
        this.score = score;
        this.winner = winner;
    }

    public GameScore getScore() {
        return score;
    }

    public Player getWinner() {
        return winner;
    }
}
