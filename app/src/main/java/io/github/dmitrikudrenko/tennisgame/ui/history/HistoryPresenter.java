package io.github.dmitrikudrenko.tennisgame.ui.history;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.data.database.DataController;
import io.github.dmitrikudrenko.tennisgame.data.model.History;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

@InjectViewState
public class HistoryPresenter extends MvpPresenter<HistoryView> {
    @Inject
    DataController dataController;

    public HistoryPresenter() {
        TennisGameApplication.injectionComponent.inject(this);
    }

    public void onAttach() {
        getViewState().showProgress();
        dataController.readHistory()
                .subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<History>() {
                            @Override
                            public void call(History history) {
                                getViewState().hideProgress();
                                getViewState().onHistoryLoad(history);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                getViewState().hideProgress();
                            }
                        }
                );
    }
}
