package io.github.dmitrikudrenko.tennisgame.ui.history;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import io.github.dmitrikudrenko.tennisgame.data.model.History;

@StateStrategyType(SingleStateStrategy.class)
public interface HistoryView extends MvpView {
    void onHistoryLoad(History history);

    void showProgress();

    void hideProgress();
}
