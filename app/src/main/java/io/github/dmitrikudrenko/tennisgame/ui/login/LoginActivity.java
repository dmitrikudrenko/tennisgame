package io.github.dmitrikudrenko.tennisgame.ui.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.auth.api.Auth;
import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import io.github.dmitrikudrenko.tennisgame.R;
import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.ui.game.GameActivity;
import io.github.dmitrikudrenko.tennisgame.ui.dialog.ProgressDialogFragment;

public class LoginActivity extends MvpAppCompatActivity implements LoginView {
    @InjectPresenter
    LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TennisGameApplication.injectionComponent.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_login);

        findViewById(R.id.sign_in_anonymously).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSignInAnonymously();
            }
        });

        findViewById(R.id.sign_in_via_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSignInViaGoogle(LoginActivity.this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            presenter.onGoogleResultReceived(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void start(Intent intent) {
        startActivityForResult(intent, 1);
    }

    @Override
    public void onSignIn() {
        startActivity(new Intent(this, GameActivity.class));
        finish();
    }

    @Override
    public void showProgress() {
        ProgressDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    public void hideProgress() {
        ProgressDialogFragment.hide(getSupportFragmentManager());
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
