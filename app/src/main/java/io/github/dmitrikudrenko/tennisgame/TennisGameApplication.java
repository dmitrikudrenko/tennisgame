package io.github.dmitrikudrenko.tennisgame;


import android.app.Application;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import io.github.dmitrikudrenko.tennisgame.injection.ApplicationComponent;
import io.github.dmitrikudrenko.tennisgame.injection.CommonInjectionModule;
import io.github.dmitrikudrenko.tennisgame.injection.DaggerApplicationComponent;
import io.github.dmitrikudrenko.tennisgame.injection.FirebaseInjectionModule;

public class TennisGameApplication extends Application {
    public static ApplicationComponent injectionComponent;

    @Inject
    FirebaseAuth firebaseAuth;

    @Override
    public void onCreate() {
        super.onCreate();
        injectionComponent = DaggerApplicationComponent.builder()
                .commonInjectionModule(new CommonInjectionModule(this))
                .firebaseInjectionModule(new FirebaseInjectionModule())
                .build();
        injectionComponent.inject(this);
        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    sendBroadcast(new Intent(BuildConfig.APPLICATION_ID + ".LOGOUT"));
                }
            }
        });
    }
}
