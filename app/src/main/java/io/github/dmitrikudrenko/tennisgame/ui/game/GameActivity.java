package io.github.dmitrikudrenko.tennisgame.ui.game;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import io.github.dmitrikudrenko.tennisgame.BuildConfig;
import io.github.dmitrikudrenko.tennisgame.R;
import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;
import io.github.dmitrikudrenko.tennisgame.ui.history.HistoryActivity;

public class GameActivity extends MvpAppCompatActivity implements GameView {
    private TextView player1Point;
    private TextView player2Point;

    @InjectPresenter
    GamePresenter presenter;

    private BroadcastReceiver logoutEventReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TennisGameApplication.injectionComponent.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_game);

        player1Point = (TextView) findViewById(R.id.player_1);
        player2Point = (TextView) findViewById(R.id.player_2);

        presenter.onAttach(this);

        findViewById(R.id.point_player_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onFirstPlayerPoint();
            }
        });

        findViewById(R.id.point_player_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSecondPlayerPoint();
            }
        });

        findViewById(R.id.undo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.undo();
            }
        });

        findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.clear();
            }
        });

        logoutEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if ((BuildConfig.APPLICATION_ID + ".LOGOUT").equals(intent.getAction())) {
                    startActivity(new Intent("io.github.dmitrikudrenko.tennisgame.LOGIN"));
                    finish();
                }
            }
        };
        registerReceiver(logoutEventReceiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".LOGOUT"));
    }

    @Override
    public void showFirstPlayerScore(String score) {
        player1Point.setText(score);
    }

    @Override
    public void showSecondPlayerScore(String score) {
        player2Point.setText(score);
    }

    @Override
    public void showActivePlayer(Player player) {
        player1Point.setTextColor(player == Player.PLAYER_1 ? Color.GREEN : Color.RED);
        player2Point.setTextColor(player == Player.PLAYER_2 ? Color.GREEN : Color.RED);
    }

    @Override
    public void showWinner(Player player) {
        Toast.makeText(GameActivity.this, player.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.m_game, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_sign_out) {
            presenter.onSettingsClick();
            return true;
        } else if (item.getItemId() == R.id.action_history) {
            startActivity(new Intent(this, HistoryActivity.class));
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(logoutEventReceiver);
        super.onDestroy();
    }
}
