package io.github.dmitrikudrenko.tennisgame.core.strategy;


import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;

public class SimpleGameStrategy implements IStrategy{
    private static final int STOP_POINT = 11;
    private static final int STEP_POINT = 2;
    private static final int MIN_DIFF = 2;

    @Override
    public Player getActivePlayer(GameScore score, Player firstPlayer, Player secondPlayer) {
        if ((score.getFirstPlayerScore() + score.getSecondPlayerScore()) % (STEP_POINT * 2) < STEP_POINT) {
            return firstPlayer;
        } else return secondPlayer;
    }

    @Override
    public Player getWinner(GameScore score, Player firstPlayer, Player secondPlayer) {
        if (score.getFirstPlayerScore() >= STOP_POINT && score.getFirstPlayerScore() - score.getSecondPlayerScore() >= MIN_DIFF) {
            return firstPlayer;
        } else if (score.getSecondPlayerScore() >= STOP_POINT && score.getSecondPlayerScore() - score.getFirstPlayerScore() >= MIN_DIFF) {
            return secondPlayer;
        } else return null;
    }
}
