package io.github.dmitrikudrenko.tennisgame.data.database;


import io.github.dmitrikudrenko.tennisgame.data.model.History;
import io.github.dmitrikudrenko.tennisgame.data.model.HistoryRecord;
import rx.Observable;

public interface DataController {
    Observable<History> readHistory();

    void saveHistoryRecord(HistoryRecord record);
}
