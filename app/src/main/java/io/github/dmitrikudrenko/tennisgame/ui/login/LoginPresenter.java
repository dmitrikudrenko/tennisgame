package io.github.dmitrikudrenko.tennisgame.ui.login;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.kelvinapps.rxfirebase.RxFirebaseAuth;

import javax.inject.Inject;

import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.analytics.Analytics;
import rx.functions.Action1;

@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView> implements GoogleApiClient.OnConnectionFailedListener {
    private FirebaseAuth firebaseAuth;
    private GoogleApiClient googleApiClient;

    @Inject
    FirebaseAnalytics analytics;

    public LoginPresenter() {
        super();
        TennisGameApplication.injectionComponent.inject(this);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                if (currentUser != null) {
                    getViewState().onSignIn();
                }
            }
        });
    }

    public void onSignInViaGoogle(FragmentActivity activity) {
        analytics.logEvent(Analytics.E_SIGN_IN_ATTEMPT_GOOGLE, null);
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("937577884425-30hn7ik8crmaja4fpidfv55s1d8migif.apps.googleusercontent.com")
                .requestEmail()
                .build();

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .enableAutoManage(activity, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                    .build();
        }

        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        getViewState().start(intent);

    }

    public void onSignInAnonymously() {
        analytics.logEvent(Analytics.E_SIGN_IN_ATTEMPT_ANONYMOUSLY, null);
        getViewState().showProgress();
        RxFirebaseAuth.signInAnonymously(firebaseAuth)
                .subscribe(
                        new Action1<AuthResult>() {
                            @Override
                            public void call(AuthResult authResult) {
                                analytics.logEvent(Analytics.E_SIGN_IN_ANONYMOUSLY, null);
                                getViewState().hideProgress();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                getViewState().showError("Authentification failed");
                                getViewState().hideProgress();
                            }
                        }
                );
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void onGoogleResultReceived(GoogleSignInResult result) {
        if (result.isSuccess()) {
            getViewState().showProgress();
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getId(), null);
                RxFirebaseAuth.signInWithCredential(firebaseAuth, credential)
                        .subscribe(
                                new Action1<AuthResult>() {
                                    @Override
                                    public void call(AuthResult authResult) {
                                        analytics.logEvent(Analytics.E_SIGN_IN_GOOGLE, null);
                                        getViewState().hideProgress();
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        getViewState().showError("Authentification failed");
                                        getViewState().hideProgress();
                                    }
                                }
                        );
            } else getViewState().hideProgress();
        } else getViewState().showError(result.getStatus().toString());
    }
}
