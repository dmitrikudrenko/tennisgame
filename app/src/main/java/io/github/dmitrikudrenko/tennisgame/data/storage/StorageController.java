package io.github.dmitrikudrenko.tennisgame.data.storage;


import android.net.Uri;

import com.google.firebase.storage.UploadTask;

import rx.Observable;

public interface StorageController {
    Observable<UploadTask.TaskSnapshot> putFile(Uri uri);

    Observable<Uri> getFile();
}
