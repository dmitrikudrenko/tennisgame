package io.github.dmitrikudrenko.tennisgame.core.strategy;


import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;

public interface IStrategy {
    Player getActivePlayer(GameScore score, Player firstPlayer, Player secondPlayer);

    Player getWinner(GameScore score, Player firstPlayer, Player secondPlayer);
}
