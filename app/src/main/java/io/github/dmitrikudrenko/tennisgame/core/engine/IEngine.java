package io.github.dmitrikudrenko.tennisgame.core.engine;


import io.github.dmitrikudrenko.tennisgame.core.model.GameResult;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;
import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import rx.Observable;

public interface IEngine {
    void init(Player firstPlayer, Player secondPlayer);

    void onFirstPlayerPoint();

    void onSecondPlayerPoint();

    void undo();

    void clear();

    boolean isFinished();

    Observable<GameScore> scoreSubscription();

    Observable<Player> activePlayerSubscription();

    Observable<GameResult> winnerSubscription();
}
