package io.github.dmitrikudrenko.tennisgame.ui.game;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import io.github.dmitrikudrenko.tennisgame.core.model.Player;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface GameView extends MvpView {
    void showFirstPlayerScore(String score);

    void showSecondPlayerScore(String score);

    void showActivePlayer(Player player);

    void showWinner(Player player);
}
