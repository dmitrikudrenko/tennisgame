package io.github.dmitrikudrenko.tennisgame.data.database;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.github.dmitrikudrenko.tennisgame.data.model.History;
import io.github.dmitrikudrenko.tennisgame.data.model.HistoryRecord;
import rx.Observable;
import rx.functions.Func1;

public class FirebaseDataController implements DataController {
    private DatabaseReference historyReference;

    public FirebaseDataController(FirebaseDatabase firebaseDatabase, FirebaseAuth firebaseAuth) {
        String uid = firebaseAuth.getCurrentUser().getUid();
        DatabaseReference userReference = firebaseDatabase.getReference(uid);
        this.historyReference = userReference.child("history");
    }

    @Override
    public Observable<History> readHistory() {
        return RxFirebaseDatabase.observeSingleValueEvent(historyReference, DataSnapshotMapper.mapOf(HistoryRecord.class))
                .map(new Func1<LinkedHashMap<String, HistoryRecord>, History>() {
                    @Override
                    public History call(LinkedHashMap<String, HistoryRecord> data) {
                        List<HistoryRecord> records = new ArrayList<>(data.size());
                        for (Map.Entry<String, HistoryRecord> row : data.entrySet()) {
                            HistoryRecord record = row.getValue();
                            record.setId(row.getKey());
                            records.add(record);
                        }
                        return new History(records);
                    }
                });
    }

    @Override
    public void saveHistoryRecord(HistoryRecord record) {
        historyReference.child(record.getId()).setValue(record);
    }
}
