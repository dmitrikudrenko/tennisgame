package io.github.dmitrikudrenko.tennisgame.ui.game;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.analytics.Analytics;
import io.github.dmitrikudrenko.tennisgame.core.engine.IEngine;
import io.github.dmitrikudrenko.tennisgame.core.engine.TennisEngine;
import io.github.dmitrikudrenko.tennisgame.core.model.GameResult;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;
import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import io.github.dmitrikudrenko.tennisgame.core.strategy.SimpleGameStrategy;
import io.github.dmitrikudrenko.tennisgame.data.database.DataController;
import io.github.dmitrikudrenko.tennisgame.data.model.HistoryRecord;
import rx.functions.Action1;

@InjectViewState
public class GamePresenter extends MvpPresenter<GameView> {
    private IEngine engine = new TennisEngine.Builder().setStrategy(new SimpleGameStrategy()).build();
    private InterstitialAd mInterstitialAd;

    @Inject
    FirebaseAnalytics analytics;

    @Inject
    FirebaseAuth firebaseAuth;

    @Inject
    DataController dataController;

    public GamePresenter() {
        super();
        TennisGameApplication.injectionComponent.inject(this);
    }

    public void onAttach(Context context) {
        configureAds(context);
        configureGame();
    }

    private void configureGame() {
        engine.activePlayerSubscription().subscribe(
                new Action1<Player>() {
                    @Override
                    public void call(Player player) {
                        getViewState().showActivePlayer(player);
                    }
                }
        );

        engine.winnerSubscription().subscribe(
                new Action1<GameResult>() {
                    @Override
                    public void call(GameResult result) {
                        analytics.logEvent(result.getWinner() == Player.PLAYER_1 ?
                                Analytics.E_GAME_WINNER_PLAYER_FIRST : Analytics.E_GAME_WINNER_PLAYER_SECOND, null);
                        getViewState().showWinner(result.getWinner());

                        dataController.saveHistoryRecord(new HistoryRecord(result.getScore()));
                    }
                }
        );

        engine.scoreSubscription().subscribe(
                new Action1<GameScore>() {
                    @Override
                    public void call(GameScore gameScore) {
                        getViewState().showFirstPlayerScore(String.valueOf(gameScore.getFirstPlayerScore()));
                        getViewState().showSecondPlayerScore(String.valueOf(gameScore.getSecondPlayerScore()));
                    }
                }
        );

        engine.init(Player.PLAYER_1, Player.PLAYER_2);
    }

    private void configureAds(Context context) {
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-8688260819723230/9864230701");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                engine.clear();
            }
        });
        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("EE14284044FB88946F1377D63C1C9242")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    public void onFirstPlayerPoint() {
        engine.onFirstPlayerPoint();
    }

    public void onSecondPlayerPoint() {
        engine.onSecondPlayerPoint();
    }

    public void undo() {
        analytics.logEvent(Analytics.E_GAME_PROCESS_UNDO, null);
        engine.undo();
    }

    public void clear() {
        analytics.logEvent(Analytics.E_GAME_PROCESS_CLEAR, null);
        if (engine.isFinished() && mInterstitialAd.isLoaded()) {
            analytics.logEvent(Analytics.E_ADS_BETWEEN_GAME, null);
            mInterstitialAd.show();
        } else engine.clear();
    }

    public void onSettingsClick() {
        firebaseAuth.signOut();
    }
}
