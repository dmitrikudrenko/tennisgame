package io.github.dmitrikudrenko.tennisgame.data.storage;


import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kelvinapps.rxfirebase.RxFirebaseStorage;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FirebaseStorageController implements StorageController {
    private StorageReference storageReference;

    public FirebaseStorageController(FirebaseAuth firebaseAuth, FirebaseStorage firebaseStorage) {
        storageReference = firebaseStorage.getReference(firebaseAuth.getCurrentUser().getUid());
    }

    @Override
    public Observable<UploadTask.TaskSnapshot> putFile(Uri uri) {
        return RxFirebaseStorage.putFile(storageReference, uri)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Uri> getFile() {
        return RxFirebaseStorage.getDownloadUrl(storageReference)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
