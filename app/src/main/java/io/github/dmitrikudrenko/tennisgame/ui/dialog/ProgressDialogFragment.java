package io.github.dmitrikudrenko.tennisgame.ui.dialog;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import io.github.dmitrikudrenko.tennisgame.R;

public class ProgressDialogFragment extends DialogFragment {
    private static final String TAG = ProgressDialogFragment.class.getName();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_loading));
        return progressDialog;
    }

    public static void show(FragmentManager fragmentManager) {
        new ProgressDialogFragment().show(fragmentManager, TAG);
    }

    public static void hide(FragmentManager fragmentManager) {
        ProgressDialogFragment fragment = (ProgressDialogFragment) fragmentManager.findFragmentByTag(TAG);
        if (fragment != null) fragment.dismiss();
    }
}
