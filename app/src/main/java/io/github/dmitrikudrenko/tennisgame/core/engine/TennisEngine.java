package io.github.dmitrikudrenko.tennisgame.core.engine;


import java.util.Stack;

import io.github.dmitrikudrenko.tennisgame.core.model.GameResult;
import io.github.dmitrikudrenko.tennisgame.core.model.Player;
import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import io.github.dmitrikudrenko.tennisgame.core.strategy.IStrategy;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class TennisEngine implements IEngine {
    private Player firstPlayer;
    private Player secondPlayer;

    private GameScore score;

    private BehaviorSubject<Player> activePlayerSubject = BehaviorSubject.create();
    private BehaviorSubject<GameResult> winnerSubject = BehaviorSubject.create();
    private BehaviorSubject<GameScore> gameScoreSubject = BehaviorSubject.create();

    private Stack<Player> history = new Stack<>();

    private boolean finished;

    private IStrategy strategy;

    public static class Builder {
        private IStrategy strategy;

        public Builder() {
        }

        public Builder setStrategy(IStrategy strategy) {
            this.strategy = strategy;
            return this;
        }

        public IEngine build() {
            return new TennisEngine(strategy);
        }
    }

    private TennisEngine(IStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public void init(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.score = new GameScore(0, 0);
        this.finished = false;
        this.history.clear();
        activePlayerSubject.onNext(firstPlayer);
        gameScoreSubject.onNext(score);
    }

    @Override
    public void onFirstPlayerPoint() {
        if (!finished) {
            score.firstPlayerPoint();
            history.push(firstPlayer);
            onPointChange();
        }
    }

    @Override
    public void onSecondPlayerPoint() {
        if (!finished) {
            score.secondPlayerPoint();
            history.push(secondPlayer);
            onPointChange();
        }
    }

    @Override
    public void undo() {
        if (!history.isEmpty()) {
            Player lastPlayer = history.pop();
            if (lastPlayer == firstPlayer) {
                score.revokeFirstPlayerPoint();
            } else score.revokeSecondPlayerPoint();
            onPointChange();
        }
    }

    @Override
    public void clear() {
        init(firstPlayer, secondPlayer);
    }

    private void onPointChange() {
        activePlayerSubject.onNext(strategy.getActivePlayer(score, firstPlayer, secondPlayer));

        gameScoreSubject.onNext(score);
        checkResult();
    }

    private void checkResult() {
        Player winner = strategy.getWinner(score, firstPlayer, secondPlayer);
        if (winner != null) {
            winnerSubject.onNext(new GameResult(score, winner));
            this.finished = true;
        } else this.finished = false;
    }

    @Override
    public Observable<Player> activePlayerSubscription() {
        return activePlayerSubject;
    }

    @Override
    public Observable<GameResult> winnerSubscription() {
        return winnerSubject;
    }

    @Override
    public Observable<GameScore> scoreSubscription() {
        return gameScoreSubject;
    }

    @Override
    public boolean isFinished() {
        return finished;
    }
}
