package io.github.dmitrikudrenko.tennisgame.core.model;


public enum Player {
    PLAYER_1("Player 1"),
    PLAYER_2("Player 2");

    private String name;

    Player(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }
}
