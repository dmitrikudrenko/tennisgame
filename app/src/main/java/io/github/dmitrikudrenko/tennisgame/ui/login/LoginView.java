package io.github.dmitrikudrenko.tennisgame.ui.login;


import android.content.Intent;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SingleStateStrategy.class)
public interface LoginView extends MvpView {
    void start(Intent intent);

    void onSignIn();

    void showProgress();

    void hideProgress();

    void showError(String message);
}
