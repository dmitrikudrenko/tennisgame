package io.github.dmitrikudrenko.tennisgame.injection;


import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.github.dmitrikudrenko.tennisgame.data.database.DataController;
import io.github.dmitrikudrenko.tennisgame.data.database.FirebaseDataController;
import io.github.dmitrikudrenko.tennisgame.data.storage.FirebaseStorageController;
import io.github.dmitrikudrenko.tennisgame.data.storage.StorageController;

@Module
public class FirebaseInjectionModule {
    @Provides
    public FirebaseAnalytics provideAnalytics(Context context) {
        return FirebaseAnalytics.getInstance(context);
    }

    @Provides
    public FirebaseAuth provideAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    public FirebaseDatabase provideDatabase() {
        return FirebaseDatabase.getInstance();
    }

    @Provides
    public FirebaseStorage provideStorage() {
        return FirebaseStorage.getInstance();
    }

    @Provides
    @Singleton
    public DataController provideDataController(FirebaseDatabase firebaseDatabase, FirebaseAuth firebaseAuth) {
        return new FirebaseDataController(firebaseDatabase, firebaseAuth);
    }

    @Provides
    @Singleton
    public StorageController provideStorageController(FirebaseStorage firebaseStorage, FirebaseAuth firebaseAuth) {
        return new FirebaseStorageController(firebaseAuth, firebaseStorage);
    }
}
