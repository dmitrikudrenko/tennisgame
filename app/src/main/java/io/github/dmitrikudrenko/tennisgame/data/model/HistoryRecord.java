package io.github.dmitrikudrenko.tennisgame.data.model;


import com.google.firebase.database.Exclude;

import java.util.Date;
import java.util.UUID;

import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;

public class HistoryRecord {
    private String id;

    private long createTimestamp;
    private GameScore gameScore;

    public HistoryRecord(GameScore gameScore) {
        this.id = UUID.randomUUID().toString();
        this.createTimestamp = new Date().getTime();
        this.gameScore = gameScore;
    }

    public GameScore getGameScore() {
        return gameScore;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreateTimestamp() {
        return createTimestamp;
    }

    public HistoryRecord() {
    }
}
