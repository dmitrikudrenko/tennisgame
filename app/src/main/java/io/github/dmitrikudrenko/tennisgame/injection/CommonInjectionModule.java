package io.github.dmitrikudrenko.tennisgame.injection;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CommonInjectionModule {
    private Context context;

    public CommonInjectionModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return context;
    }
}
