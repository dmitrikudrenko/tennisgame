package io.github.dmitrikudrenko.tennisgame.data.model;


import java.util.List;

public class History {
    private List<HistoryRecord> data;

    public History(List<HistoryRecord> data) {
        this.data = data;
    }

    public List<HistoryRecord> getData() {
        return data;
    }
}
