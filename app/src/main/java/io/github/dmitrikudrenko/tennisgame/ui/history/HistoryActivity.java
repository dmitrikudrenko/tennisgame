package io.github.dmitrikudrenko.tennisgame.ui.history;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Locale;

import io.github.dmitrikudrenko.tennisgame.R;
import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.core.model.GameScore;
import io.github.dmitrikudrenko.tennisgame.data.model.History;
import io.github.dmitrikudrenko.tennisgame.data.model.HistoryRecord;
import io.github.dmitrikudrenko.tennisgame.ui.dialog.ProgressDialogFragment;

public class HistoryActivity extends MvpAppCompatActivity implements HistoryView {
    @InjectPresenter
    HistoryPresenter presenter;

    private HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TennisGameApplication.injectionComponent.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_history);
        injectViews();

        presenter.onAttach();
    }

    private void injectViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        historyAdapter = new HistoryAdapter();
        recyclerView.setAdapter(historyAdapter);
    }

    @Override
    public void onHistoryLoad(History history) {
        historyAdapter.setData(history);
    }

    @Override
    public void showProgress() {
        ProgressDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    public void hideProgress() {
        ProgressDialogFragment.hide(getSupportFragmentManager());
    }

    private static class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryRecordViewHolder> {
        private History history;

        @Override
        public HistoryRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new HistoryRecordViewHolder(LayoutInflater
                    .from(parent.getContext()).inflate(R.layout.v_history_item, parent, false));
        }

        @Override
        public void onBindViewHolder(HistoryRecordViewHolder holder, int position) {
            holder.bind(history.getData().get(position));
        }

        @Override
        public int getItemCount() {
            return history != null && history.getData() != null ? history.getData().size() : 0;
        }

        public void setData(History history) {
            // TODO: 11.01.2017 use DiffUtils
            this.history = history;
            notifyDataSetChanged();
        }

        static class HistoryRecordViewHolder extends RecyclerView.ViewHolder {

            public HistoryRecordViewHolder(View itemView) {
                super(itemView);
            }

            public void bind(HistoryRecord record) {
                GameScore gameScore = record.getGameScore();
                ((TextView) itemView).setText(String.format(Locale.getDefault(), "%d - %d", gameScore.getFirstPlayerScore(), gameScore.getSecondPlayerScore()));
            }
        }
    }
}
