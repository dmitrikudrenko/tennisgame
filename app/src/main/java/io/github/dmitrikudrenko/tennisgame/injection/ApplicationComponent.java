package io.github.dmitrikudrenko.tennisgame.injection;


import javax.inject.Singleton;

import dagger.Component;
import io.github.dmitrikudrenko.tennisgame.TennisGameApplication;
import io.github.dmitrikudrenko.tennisgame.ui.game.GameActivity;
import io.github.dmitrikudrenko.tennisgame.ui.game.GamePresenter;
import io.github.dmitrikudrenko.tennisgame.ui.history.HistoryActivity;
import io.github.dmitrikudrenko.tennisgame.ui.history.HistoryPresenter;
import io.github.dmitrikudrenko.tennisgame.ui.login.LoginActivity;
import io.github.dmitrikudrenko.tennisgame.ui.login.LoginPresenter;

@Singleton
@Component(modules = {CommonInjectionModule.class, FirebaseInjectionModule.class})
public interface ApplicationComponent {
    void inject(TennisGameApplication application);

    void inject(LoginActivity activity);

    void inject(GameActivity activity);

    void inject(HistoryActivity activity);

    void inject(LoginPresenter presenter);

    void inject(GamePresenter presenter);

    void inject(HistoryPresenter presenter);
}
