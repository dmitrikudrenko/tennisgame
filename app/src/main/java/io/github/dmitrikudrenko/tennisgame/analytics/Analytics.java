package io.github.dmitrikudrenko.tennisgame.analytics;


public class Analytics {
    public static final String E_SIGN_IN_ATTEMPT_GOOGLE = "E_SIGN_IN_ATTEMPT_GOOGLE";
    public static final String E_SIGN_IN_ATTEMPT_ANONYMOUSLY = "E_SIGN_IN_ATTEMPT_ANONYMOUSLY";
    
    public static final String E_SIGN_IN_GOOGLE = "E_SIGN_IN_GOOGLE";
    public static final String E_SIGN_IN_ANONYMOUSLY = "E_SIGN_IN_ANONYMOUSLY";
    
    public static final String E_GAME_WINNER_PLAYER_FIRST = "E_GAME_WINNER_PLAYER_FIRST";
    public static final String E_GAME_WINNER_PLAYER_SECOND = "E_GAME_WINNER_PLAYER_SECOND";
    
    public static final String E_GAME_PROCESS_UNDO = "E_GAME_PROCESS_UNDO";
    public static final String E_GAME_PROCESS_CLEAR = "E_GAME_PROCESS_CLEAR";
    
    public static final String E_ADS_BETWEEN_GAME = "E_ADS_BETWEEN_GAME";
}
