package io.github.dmitrikudrenko.tennisgame.core.model;


public class GameScore {
    private int firstPlayerScore;
    private int secondPlayerScore;

    public GameScore() {
    }

    public GameScore(int firstPlayerScore, int secondPlayerScore) {
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
    }

    public int getFirstPlayerScore() {
        return firstPlayerScore;
    }

    public int getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public void firstPlayerPoint() {
        firstPlayerScore++;
    }

    public void secondPlayerPoint() {
        secondPlayerScore++;
    }

    public void revokeFirstPlayerPoint() {
        firstPlayerScore--;
    }

    public void revokeSecondPlayerPoint() {
        secondPlayerScore--;
    }
}
